#! /usr/bin/env python
# -*- coding: utf8 -*-

"""
PyGalleryCrawler is a web crawler for online gallery aka TGP downloader.

Copyright (c) 2006 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

#TODO : gerer les noms avec %20 et autres ...
#TODO : gestion des 403 et  autres ...
#TODO : gerer la desactivation de psyco si lance depuis wing
#TODO : overide de la config donné avec -c pour rewrite et debug

# import system modules
import sys
import os
import os.path

# import utility modules
import re
import difflib
import shutil

# import internet modules
import urllib2
import urlparse
import urllib2, urlparse, gzip
from StringIO import StringIO

def log(level, message):
    """
    Take care of log.
    Before some import to have it.
    0:debug, 1:info, 2:warning, 3:error
    """
    message_level = ["debug", "info", "warning", "error"]
    if level==1:
        print message
    elif level==4:
        print >> sys.stderr, "%s: %s" % (message_level[4], message)
    else:
        print "%s: %s" % (message_level[level], message)

# can improve the regexp manipulation, but I'm not sure :D
#try:
    #import psyco
    #psyco.full()
#except:
    #log(2, "Psyco module is not installed")
    #pass

# import the default config
import config

try:
    import Image
    config.pil = 1
except:
    log(2, "Python Image Library module is not installed (no check on pictures and no thumbnails generation)")
    config.pil = 0

# CONFIGURATION
################################################################################

# options from the command line
from optparse import OptionParser
usage = """%prog [options] source"""
parser = OptionParser(usage=usage, version="PyGalleryCrawler %s" % (config.version))
parser.add_option("-c","--config",
        action="store", type="string", dest="config", metavar="FILE", help="use FILE for the config")
parser.add_option("-f","--feed",
        action="store_true", dest="feed", default=False, help="the source is a feed")
parser.add_option("-o","--overwrite",
        action="store_true", dest="overwrite", default=False, help="overwrite the crawling even if it's seems already done")
parser.add_option("-d","--debug",
        action="store_true", dest="debug", default=False, help="what's happens inside ?")
parser.add_option("-m","--method",
        action="store", dest="method", default=None, help="use a special method, check r_method in source code")
(options, args) = parser.parse_args()
if len(args) == 0:
    parser.error("incorrect number of arguments")

# when a personnal config file is given by -c, insert values in config
if options.config:
    glob = {}
    loc = {}
    execfile(options.config, glob, loc)
    for key in loc.keys():
        config.__dict__[key] = loc[key]

# insert command line's options in config
config.data_force_overwrite = options.overwrite
config.feed = options.feed
config.debug = options.debug
if options.method == None:
    config.method = None
else:
    config.method = int(options.method)

# the main option
config.source_root = args[0]

if config.feed:
    try:
        import feedparser
    except:
        log(3, "Impossible to read a feed without the python module feedparser")
        sys.exit(-1)

# VARIABLES
################################################################################

# all the regex to find links
# - minus the </a> cause some webmaster don't put it in some tables and the navigator handle it
r_image_direct_thumb_direct = re.compile(r'<a[^<>]*?href=[\'"]{,1}([^ <>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>[^<>]*?<img[^<>]*?src=[\'"]{,1}([^<>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>', re.IGNORECASE)
r_image_direct_thumb_direct_dotall = re.compile(r'<a[^<>]*?href=[\'"]{,1}([^ <>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>[^<>]*?<img[^<>]*?src=[\'"]{,1}([^<>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>', re.IGNORECASE | re.DOTALL)
r_image_link_thumb_direct = re.compile(r'<a[^<>]*?href=[\'"]{,1}([^ <>\'"]*)[\'"]{,1}[^<>]*?>[^<>]*?<img[^<>]*?src=[\'"]{,1}([^<>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>', re.IGNORECASE)
r_image_inside_thumb_none = re.compile(r'<img[^<>]*?src=[\'"]{,1}([^ <>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>', re.IGNORECASE)

# special method from --method, check also find_links()
r_method = []
# 0 : take the a and img with tags inside like <a <span><img></span>>, regexp from r_image_link_thumb_direct
# example : 
r_method.append(re.compile(r'<a[^<>]*?href=[\'"]{,1}([^ <>\'"]*)[\'"]{,1}[^<>]*?>.*?<img[^<>]*?src=[\'"]{,1}([^<>?]*?\.(jpg|gif))[\'"]{,1}[^<>]*?>', re.IGNORECASE))

html_header = """<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type" />
<title>PyGalleryCrawler</title>
<style type="text/css">a img {border: none;}</style>
</head>
<body>
"""

html_footer = """<p>
<a href="http://wingware.com"><img border="0" src="http://wingware.com/images/coded_w_wing_small.png" alt="Wingware logo" ></a>
<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
</p>
</body>
</html>
"""

# FUNCTIONS
################################################################################

def main():
    """
    The core is near you
    """
    if config.feed:
        log(2, 'still "works in progress"')
        d = feedparser.parse(config.source_root)

        for i in range(len(d['entries'])):
            crawl(d['entries'][i]['link'])
    else:
        crawl(config.source_root)
    if config.beep:
        log(1, "Have a nice day!\a")

def crawl(source_root):
    # check and prepare the different paths for saving
    log(1, "crawling '%s'" % (source_root))
    source_parse = urlparse.urlparse(source_root)
    source_root_validname = source_parse[1]+'__'+"".join([source_parse[2],source_parse[3],source_parse[4]]).replace("/","_")
    main_target_path = os.path.join(config.data_directory, source_root_validname)
    if os.path.isdir(main_target_path):
        if config.data_force_overwrite:
            log(1, "emptying '%s'" % (main_target_path))
            shutil.rmtree(main_target_path)
        else:
            log(3, "the directory '%s' already exists" % (main_target_path))
            return
    else:
        log(1, "create the main directory '%s'" % (main_target_path))
        os.makedirs(main_target_path)
    if config.pictures_download:
        pictures_target_path = os.path.join(main_target_path, config.pictures_directory)
        if not os.path.isdir(pictures_target_path):
            os.makedirs(pictures_target_path)
    if config.thumbnails_download:
        thumbnails_target_path = os.path.join(main_target_path, config.thumbnails_directory)
        if not os.path.isdir(thumbnails_target_path):
            os.makedirs(thumbnails_target_path)
    if config.index_create:
        index_target_path = os.path.join(main_target_path, config.index_directory)
        index_target_file = os.path.join(index_target_path, config.index_filename)
        if not os.path.isdir(index_target_path):
            os.makedirs(index_target_path)
        index = open(index_target_file, "w")
        index.write(html_header)
        index.write('<p>Crawled by PyGalleryCrawler from  <a href="%s">%s</a></p>\n' % (source_root, source_root))
        index.write('<p>\n')
    if config.debug:
        debug_target_path = os.path.join(main_target_path, config.debug_directory)
        if not os.path.isdir(debug_target_path):
            os.makedirs(debug_target_path)
        if config.debug_url_file_create:
            url_file = open(os.path.join(debug_target_path, config.debug_url_filename), "w")
            url_file.write(source_root)
            url_file.close()

    # fetch the main html source
    log(1, "download the source '%s'" % (source_root))
    result = fetch(source_root)

    # save the html source if needed
    if result.has_key('url') and config.debug:
        target_path = os.path.join(debug_target_path, config.debug_source_filename)
        log(0, "write source in %s" % (target_path))
        fo = open(target_path, "w")
        fo.write(result['data'])
        fo.close()
        log(0, "source size %d" % (len(result['data'])))

    # crawl in links
    links = find_links(result)
    if len(links) == 0:
        log(1, "Nothing found :/")
        return
    else:
        log(1, "%d images found :)" % (len(links)))

    # from an url or a file ?
    if result.has_key('url'):
        counter = 1
        counter_really_dl = 0
        counter_max = len(links)
        for i in links:
            if i[1]!='None':
                counter_max += 1
        for i in links:
            # dowload pictures
            if config.pictures_download:
                log(1, "[%d/%d] download picture '%s'" % (counter, counter_max, i[0]))
                p_path = download(i[0], pictures_target_path, source_root)
                counter += 1
                # check size
                if config.pictures_check_size and config.pil:
                    try:
                        im = Image.open(p_path)
                    except IOError, err:
                        log(2, "PIL can't read '%s' (%s)" % (p_path, ", ".join(err)))
                        if config.debug:
                            shutil.move(p_path, debug_target_path)
                        else:
                            os.remove(p_path)
                        continue
                    else:
                        if config.debug:
                            log(0, "image size : %dx%d" % (im.size[0], im.size[1]))
                        if im.size[0] < config.pictures_min_width or\
                            im.size[1] < config.pictures_min_height:
                            log(2, "clean out, '%s' is too small ..." % (p_path))
                            os.remove(p_path)
                            continue
            # dowload thumbnails
            if config.thumbnails_download and not i[1]=='None':
                log(1, "[%d/%d] download thumbnail '%s'" % (counter, counter_max, i[1]))
                t_path = download(i[1], thumbnails_target_path, source_root)
                counter += 1
                # check size
                if config.thumbnails_check_size and config.pil:
                    try:
                        im = Image.open(t_path)
                    except IOError, err:
                        log(2, "PIL can't read '%s' (%s)" % (t_path, ", ".join(err)))
                        if config.debug:
                            shutil.move(t_path, debug_target_path)
                        else:
                            os.remove(t_path)
                        if config.pictures_download:
                            os.remove(p_path)
                        continue
                    else:
                        if config.debug:
                            log(0, "image size : %dx%d" % (im.size[0], im.size[1]))
                        if im.size[0] < config.thumbnails_min_width or\
                            im.size[1] < config.thumbnails_min_height:
                            log(2, "clean out, '%s' is too small ..." % (t_path))
                            os.remove(t_path)
                            if config.pictures_download:
                                os.remove(p_path)
                            continue
            # create thumbnails if none
            elif config.thumbnails_download and i[1]=='None' and config.thumbnails_create and config.pil:
                ct_filename = extract_filename_from_url(i[0])
                ct_path = os.path.join(thumbnails_target_path, ct_filename)
                try:
                    im = Image.open(p_path)
                    im.thumbnail([config.thumbnails_max_width, config.thumbnails_max_height])
                    im.save(ct_path, "JPEG")
                    i[1]= ct_filename
                    log(1, "thumbnail created at '%s'" % (ct_path))
                except IOError, err:
                    log(2, "PIL can't create '%s' from '%s' (%s)" % (ct_path, p_path, ", ".join(err)))

            # there is a pic and his thumbnail
            counter_really_dl += 1

            # create a local index.html in the directory
            if config.index_create:
                if i[1]=='None':
                    index.write('<a href="%s">%s</a>\n' % \
                        (os.path.join(config.pictures_directory, extract_filename_from_url(i[0])),\
                        extract_filename_from_url(i[0])))
                else:
                    index.write('<a href="%s"><img src="%s" alt="%s"/></a>\n' % \
                        (os.path.join(config.pictures_directory, extract_filename_from_url(i[0])),\
                        os.path.join(config.thumbnails_directory, extract_filename_from_url(i[1])),\
                        extract_filename_from_url(i[1])))
        if config.index_create:
            index.write('<p>%d image(s)</p>\n' % (counter_really_dl))
            index.write('</p>\n')
            index.write(html_footer)
            index.close()
            log(1, "local gallery created at '%s'" % (index_target_file))
        log(1, "downloaded in '%s'" % (main_target_path))
        if config.index_open_after:
            cmd = config.index_open_after_cl % (index_target_file)
            os.system(cmd)
    else:
        for i in links:
            print 'I:%s:T:%s' % (i[0], i[1])

def find_links(result):
    """
    Main entry for call different find function
    """
    links = []
    if config.method == None:
        log(1, "crawling with simple links to images and thumbnails ...")
        links = find_image(result, r_image_direct_thumb_direct)
        if len(links)!=0:
            return links
        log(1, "crawling with simple links to images and thumbnails and dotall (cpu burner) ...")
        links = find_image(result, r_image_direct_thumb_direct_dotall)
        if len(links)!=0:
            return links
        log(1, "crawling with images links and direct thumbnails (uber cpu burner) ...")
        links = find_image(result, r_image_link_thumb_direct, links_to_images=1)
        if len(links)!=0:
            return links
        log(1, "crawling with direct images and no thumbnails ...")
        links = find_image(result, r_image_inside_thumb_none, assume_thumbnails=0)
        if len(links)!=0:
            return links
    else:
        log(1, "crawling with the special method number %d" % (config.method))
        if config.method in [0]:
            links = find_image(result, r_method[config.method], links_to_images=1)
        else:
            links = find_image(result, r_method[config.method])
    log(1, "end of crawling")
    return links

def find_image(result, re_used, assume_thumbnails=1, links_to_images=0):
    """
    Use the force Luke!

    result : the object with the links, html, etc.
    re_used : the regexp to extract the links
    assume_thumbnails : we think that there is some thumbnails
    links_to_images : the first link is an "a" tag, the regexp is different
    """
    # use the regexp to find all links
    links_re = re.findall(re_used, result['data'])
    if len(links_re)==0:
        if config.debug:
            log(0, "nothing found with the regexp")
        return []

    # keep only links, not temporary regexp groups
    l_out = []
    for i in range(len(links_re)):
        if assume_thumbnails:
            if not links_to_images:
                l_out.append(\
                    [absolute_url(result.get('url', 'None'), links_re[i][0]),\
                    absolute_url(result.get('url', 'None'), links_re[i][2])])
            else:
                l_out.append(\
                    [absolute_url(result.get('url', 'None'), links_re[i][0]),\
                    absolute_url(result.get('url', 'None'), links_re[i][1])])
        else:
            l_out.append(\
                [absolute_url(result.get('url', 'None'), links_re[i][0]),\
                'None'])
    if config.debug:
        log(0, "all interesting links found (%d)" % (len(l_out)))
        log(0, l_out)
        log(0, "config.lc_diff_ratio_limit=%f" % (config.lc_diff_ratio_limit))
        log(0, "config.lc_similar_links_minimum=%d" % (config.lc_similar_links_minimum))

    # warn people about horrible pages :)
    if len(l_out)>100:
        log(2, "%d links to check, means %d diff tests! Can distract your CPU few seconds :)" % (len(l_out), len(l_out)*(len(l_out)-1)))

    # compute similarity in result of the regexp for each links to the other
    l_in = l_out
    l_out = []
    for i in range(len(l_in)):
        similar = 0
        for j in range(len(l_in)):
            if i != j:
                if l_in[i][0]==l_in[j][0]:
                    # not a good news to have similar links ...
                    continue
                diff = difflib.SequenceMatcher(None, l_in[i][0], l_in[j][0]).ratio()
                if diff > config.lc_diff_ratio_limit:
                    similar += 1
                if config.debug:
                    log(0, "\t%s <=> %s === %f" % (l_in[i][0], l_in[j][0], diff))
	if config.debug:
            log(0, "similar = %d" % (similar))
        if similar >= config.lc_similar_links_minimum:
            l_out.append([l_in[i][0], l_in[i][1]])
            if config.debug:
                log(0, "%s have similarity with %d links" % (l_in[i][0], similar))
    if config.debug:
        log(0, "interesting links keeped (%d)" % (len(l_out)))
        log(0, l_out)

    if len(l_out)<config.lc_minimal_links_number:
        return []

    # if images are direct links, it's done
    if not links_to_images:
        return l_out
    else:
        log(1, "%d interesting links found" % (len(l_out)))

    # fetch all temporaries pages
    """
    I don't keep a list of exit pages cause if I keep a list of redirections pages
    and disable any pages that look like the same and if the same page is used to go to
    the real gallery, every page of the gallerry will be out
    """
    l_in = l_out
    l_out_result = []
    counter = 1
    counter_max = len(l_in)
    for i in range(counter_max):
        # get the page
        log(1, "[%d/%d] download temp file '%s'" % (counter, counter_max, l_in[i][0]))
        result = fetch(l_in[i][0])
        counter += 1
        # test if the original link is the same than the final url ?
        if l_in[i][0] != result['url']:
            tmp_source = urlparse.urlparse(result['url'])
            if tmp_source[2]=='' and tmp_source[3]=='' and tmp_source[4]=='':
                log(2, "clean out, '%s' seems to be a redirection to the portal '%s'" % (l_in[i][0], result['url']))
                continue
        l_out_result.append(result)
        l_out_result[-1]['pic_thumb'] = l_in[i]

    # new similarity test due to redirections and links detections in temp pages
    garbage = []
    for i in range(len(l_out_result)):
        similar = 0
        for j in range(len(l_out_result)):
            if i != j:
                diff = difflib.SequenceMatcher(None, l_out_result[i]['url'], l_out_result[j]['url']).ratio()
                if diff > config.lc_diff_ratio_limit:
                    similar += 1
        if similar < config.lc_similar_links_minimum:
            garbage.append(i)
            log(2, "clean out, '%s' seems to be garbage" % (l_out_result[i]['url']))
            continue
        # get all the links of the temporary page
        image_links = re.findall(r_image_inside_thumb_none, l_out_result[i]['data'])
        l_out_result[i]['image_links'] = []
        for j in range(len(image_links)):
            l_out_result[i]['image_links'].append([absolute_url(l_out_result[i]['url'], image_links[j][0]), 0])
            if config.debug:
                log(0, "absolute = url + link : %s = %s + %s" % (absolute_url(l_out_result[i]['url'], image_links[j][0]), l_out_result[i]['url'], image_links[j][0]))
    # delete in l_in in reverse order cause each delete change indexes except if we start by the end
    garbage.reverse()
    for i in garbage:
        del l_out_result[i]

    # compute all images between all images in each temporary pages
    probably_banners = {}
    banners = []
    empty = []
    counter = 1
    counter_max = len(l_out_result)
    lc_multiplier_link_to_be_banner = counter_max*config.lc_multiplier_link_to_be_banner
    for a in range(counter_max):
        log(1, "[%d/%d] compute temp file '%s'" % (counter, counter_max, l_out_result[a]['url']))
        counter += 1
        if l_out_result[a]['image_links']==[]:
            if config.debug:
                log(2, "pass %s : no links" % (l_out_result[a]['url']))
            empty.append(a)
            continue
        if config.debug:
            log(0, l_out_result[a]['image_links'])
        for b in range(counter_max):
            if a != b:
                if config.debug:
                    log(0, "with temporary '%s'" % (l_out_result[b]['url']))
                    log(0, l_out_result[b]['image_links'])
                if l_out_result[b]['image_links']==[]:
                    if config.debug:
                        log(2, "pass %s : no links" % (l_out_result[b]['url']))
                        continue
                for i in range(len(l_out_result[a]['image_links'])):
                    link_ai = l_out_result[a]['image_links'][i][0]
                    if link_ai in banners:
                        if config.debug:
                            log(0, "banner jump by '%s'" % (link_ai))
                        continue
                    for j in range(len(l_out_result[b]['image_links'])):
                        link_bj = l_out_result[b]['image_links'][j][0]
                        if link_bj in banners:
                            if config.debug:
                                log(0, "banner jump by '%s'" % (link_bj))
                            continue
                        if  link_ai == link_bj:
                            # probably banner but not sure cause some pages have links to the other
                            # pages
                            probably_banners[link_ai] = probably_banners.get(link_ai, 0) + 1
                            continue
                        diff = difflib.SequenceMatcher(None, link_ai, link_bj).ratio()
                        if config.debug:
                            log(0, "\t%s <=> %s === %f" % (link_ai, link_bj, diff))
                        if diff > config.lc_diff_ratio_limit:
                            l_out_result[a]['image_links'][i][1] += 1
                    # check score of probably banner after each link of the source page
                    # too much same link can't be a normal link or a next/previous link in a gallery
                    for k in probably_banners.keys():
                        if probably_banners[k] > config.lc_equal_to_be_banner:
                            if config.debug:
                                log(0, "new banner by equality '%s'" % (k))
                            del probably_banners[k]
                            banners.append(k)
                    if config.debug:
                        log(0, "banners")
                        log(0, banners)
        # links with too much similarity can be banners too
        for i in l_out_result[a]['image_links']:
            if i[1] > lc_multiplier_link_to_be_banner:
                banners.append(i[0])
                if config.debug:
                    log(0, "new banner by similarity '%s'" % (i[0]))
        # now we have to check the score of similarity
        if config.debug:
            log(0, "find better score for each page")
        max = -1
        link_chosen = ''
        for i,j in l_out_result[a]['image_links']:
            if config.debug:
                log(0, "'%s' : %d" % (i, j))
            if i not in banners and j > max and j < counter_max:
                # j < counter_max + 5 cause some ads are on each pages but have 1.jpg, 2.jpg, etc.
                max = j
                link_chosen = i
        l_out_result[a]['pic_thumb'] = [link_chosen, l_out_result[a]['pic_thumb'][1]]
        if config.debug:
            log(0, "the chosen one '%s' : %d" % (link_chosen, max))
    # delete every pages without links
    empty.reverse()
    for i in empty:
        del l_out_result[i]

    # similarity again with finals result of choosen pictures to put back limit result (often in start of the process)
    if config.debug:
        log(0, "last (we all hope) similarity test")
    l_out = []
    counter = 0
    for a in range(len(l_out_result)):
        max = -1
        for i,j in l_out_result[a]['image_links']:
            if i not in banners and j < counter_max:
                similar = 0
                for k in range(len(l_out_result)):
                    diff = difflib.SequenceMatcher(None, i, l_out_result[k]['pic_thumb'][0]).ratio()
                    if diff > config.lc_diff_ratio_limit:
                        similar += 1
                if similar > max:
                    max = similar
                    link_chosen = i
        l_out.append([link_chosen, l_out_result[a]['pic_thumb'][1]])
        counter += 1
    if config.debug:
        log(0, "cleaner_tmp")
        log(0, l_out)
    
    return l_out

def absolute_url(source, url):
    """
    Redefine relative to absolute
    ../../image.jpg -> http://www.example.com/example/image/image.jpg
    """
    url = url.strip()
    url = url.strip('\'"')
    url = url.strip('/')
    if source and not url == 'None':
        if url[:2] == '..':
            url = url[2:]
        return urlparse.urljoin(source, url)
    else:
        return url

def extract_filename_from_url(url):
    return url.split("/")[-1]

class SmartRedirectHandler(urllib2.HTTPRedirectHandler):
    def http_error_301(self, req, fp, code, msg, headers):
        result = urllib2.HTTPRedirectHandler.http_error_301(
            self, req, fp, code, msg, headers)
        result.status = code
        return result

    def http_error_302(self, req, fp, code, msg, headers):
        result = urllib2.HTTPRedirectHandler.http_error_302(
            self, req, fp, code, msg, headers)
        result.status = code
        return result

class DefaultErrorHandler(urllib2.HTTPDefaultErrorHandler):
    def http_error_default(self, req, fp, code, msg, headers):
        result = urllib2.HTTPError(
            req.get_full_url(), code, msg, headers, fp)
        result.status = code
        return result

def openAnything(source, etag=None, lastmodified=None, agent=config.user_agent, referer=None):
    '''
    From the great, uber and excellent : http://diveintopython.org

    URL, filename, or string --> stream

    This function lets you define parsers that take any input source
    (URL, pathname to local or network file, or actual data as a string)
    and deal with it in a uniform manner.  Returned object is guaranteed
    to have all the basic stdio read methods (read, readline, readlines).
    Just .close() the object when you're done with it.

    If the etag argument is supplied, it will be used as the value of an
    If-None-Match request header.

    If the lastmodified argument is supplied, it must be a formatted
    date/time string in GMT (as returned in the Last-Modified header of
    a previous request).  The formatted date/time will be used
    as the value of an If-Modified-Since request header.

    If the agent argument is supplied, it will be used as the value of a
    User-Agent request header.
    '''

    if hasattr(source, 'read'):
        return source

    if source == '-':
        return sys.stdin

    if urlparse.urlparse(source)[0] == 'http':
        # open URL with urllib2
        request = urllib2.Request(source)
        request.add_header('User-Agent', agent)
        if etag:
            request.add_header('If-None-Match', etag)
        if lastmodified:
            request.add_header('If-Modified-Since', lastmodified)
        request.add_header('Accept-encoding', 'gzip')
        if referer:
            request.add_header('Referer', referer)

        opener = urllib2.build_opener(SmartRedirectHandler(), DefaultErrorHandler())
        try:
            return opener.open(request)
        except AttributeError, reason:
            log(3,"AttributeError(%s) %s with %s" %(", ".join(reason), source))
            sys.exit(-1)
        except urllib2.URLError, err:
            log(3,"URLError(%s) %s with %s" %(", ".join(err.reason), source))
            sys.exit(-1)
        except httplib.BadStatusLine, line:
            log(3,"BadStatusLine(%s) %s with %s" %(err.reason[0], err.reason[1],source))
            sys.exit(-1)

    # try to open with native open function (if source is a filename)
    try:
        log(1, "Load from '%s'" % (source))
        return open(source)
    except (IOError, OSError):
        pass

    # treat source as string
    return StringIO(str(source))

def fetch(source, etag=None, last_modified=None, agent=config.user_agent, referer=None):
    '''
    Fetch data and metadata from a URL, file, stream, or string
    '''
    result = {}
    f = openAnything(source, etag, last_modified, agent, referer)
    result['data'] = f.read()
    if hasattr(f, 'headers'):
        # save ETag, if the server sent one
        result['etag'] = f.headers.get('ETag')
        # save Last-Modified header, if the server sent one
        result['lastmodified'] = f.headers.get('Last-Modified')
        if f.headers.get('content-encoding', '') == 'gzip':
            # data came back gzip-compressed, decompress it
            result['data'] = gzip.GzipFile(fileobj=StringIO(result['data'])).read()
    if hasattr(f, 'url'):                                            
        result['url'] = f.url                                        
        result['status'] = 200      
    if hasattr(f, 'status'):
        result['status'] = f.status
    f.close()
    return result

def download(source, target_path, referer=None):
    if not referer:
        referer=source
    result = fetch(source, referer=referer)
    source_parse = urlparse.urlparse(source)
    source_path = os.path.join(target_path, extract_filename_from_url(source_parse[2]))
    if config.debug:
        log(0, "Write %s in %s" % (source, source_path))
    fo = open(source_path, "w")
    fo.write(result['data'])
    fo.close
    if config.debug:
        log(0, "%d bytes wrote" % (len(result['data'])))
    return source_path

if __name__ == '__main__':
    main()
