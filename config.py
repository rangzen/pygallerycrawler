# general
################################################################################
#make a beep when finished, don't have to stay tune on the log
beep = 1
version = '0.1.1'

# downloader
################################################################################
# what's my name when I crawl website
user_agent = 'PyGalleryCrawler/%s' % (version)

# database
################################################################################
# database filename
database_file = 'db.sqlite'

# data
################################################################################
# main directory for downloading
data_directory = 'data'
# if a directory exists, erase it and crawl from start
data_force_overwrite = 0

# debug
################################################################################
# debug mode
debug = 0
# directory to debug files
debug_directory = 'debug'
# filename for the source
debug_source_filename = 'source.html'
# do I create a file with the source url
debug_url_file_create = 1
# filename of the url file
debug_url_filename = 'source.url'

# index for each directory
################################################################################
# do I create an html index file
index_create = 1
# in wich directory
index_directory = ''
# with what filename
index_filename = 'index.html'
# do I open it when finished ?
index_open_after = 0
# with what ?
index_open_after_cl = 'firefox %s'

# pictures
################################################################################
# do I download the pictures
pictures_download = 1
# in wich directory
pictures_directory = ''
# do I check picture size
pictures_check_size = 1
# what is the minimal width for an image
pictures_min_width = 320
# what is the minimal height for an image
pictures_min_height = 240

# thumbnails
################################################################################
# do I download thumbnails
thumbnails_download = 1
# in wich directory
thumbnails_directory = 'thumbnails'
# do I check thumbnails size
thumbnails_check_size = 1
# what is the minimal width for an image
thumbnails_min_width = 64
# what is the minimal height for an image
thumbnails_min_height = 48

# do I create thumnails if there isn't in the original source
thumbnails_create = 1
# what the max width of the thumbnail create
thumbnails_max_width = 150
# what the max height of the thumbnail create
thumbnails_max_height = 150

# links crawler
################################################################################
# minimum links to be an interesting gallery
lc_minimal_links_number = 5
# diff result limit to be counted in similarity
lc_diff_ratio_limit = 0.85
# number of count of similarity links in a page to keep it (equal or superior)
lc_similar_links_minimum = 5
# number of links which are equals to be a banner
lc_equal_to_be_banner = 4
# multiplier of number of pages compared with max similarity
lc_multiplier_link_to_be_banner = 3
